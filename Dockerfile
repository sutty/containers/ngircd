FROM sutty/monit:latest
MAINTAINER "f <f@sutty.nl>"

RUN apk add --no-cache ngircd
COPY ./monit.conf /etc/monit.d/ngircd.conf
RUN monit -t

VOLUME /etc/letsencrypt
EXPOSE 6697
